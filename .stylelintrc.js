module.exports = {
  customSyntax: 'postcss-html',
  extends: [
    'stylelint-config-html/vue',
    'stylelint-config-recess-order',
    'stylelint-config-css-modules',
  ],
  rules: {
  },
}
