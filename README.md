# [Zac Pez Portfolio](zacpez.com)

[![Project Status: Concept – Minimal or no implementation has been done yet, or the repository is only intended to be a limited example, demo, or proof-of-concept.](https://www.repostatus.org/badges/latest/concept.svg)](https://www.repostatus.org/#concept)

A personal portfolio website based on [vitesse-lit](https://github.com/antfu/vitesse-lite) template.

## TODOs

* Recreate projects page
* Clean up half-implemented data defined UI, make it all some kind of json source.
