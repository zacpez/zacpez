/* eslint-disable */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
export {}

/* prettier-ignore */
declare module 'vue' {
  export interface GlobalComponents {
    Article: typeof import('./src/components/Article.vue')['default']
    Bio: typeof import('./src/components/Bio.vue')['default']
    Footer: typeof import('./src/components/Footer.vue')['default']
    Header: typeof import('./src/components/Header.vue')['default']
    Info: typeof import('./src/components/Info.vue')['default']
    MdiMoonWaningCrescent: typeof import('~icons/mdi/moon-waning-crescent')['default']
    MdiWeatherSunny: typeof import('~icons/mdi/weather-sunny')['default']
    Project: typeof import('./src/components/Project.vue')['default']
    ResumeTimeline: typeof import('./src/components/ResumeTimeline.vue')['default']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    Skills: typeof import('./src/components/Skills.vue')['default']
  }
}
