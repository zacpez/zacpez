export const isDark = useDark()
export const toggleDark = useToggle(isDark)
isDark.value = true
