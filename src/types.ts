export interface Article {
  id: string
  title: string
  author: string
  date: string
  body: string
}

export interface Repo {
  id: string
  url: string
  title: string
  updatedAt: string
  author: string
  description: string
  issueCount: number
  issueLink: string
  watcherCount: number
  watcherLink: string
  forkCount: number
  forkLink: string
  wikiLink: string
}

export interface Project {
  id: string
  name: string
  status: string
  type: string
  repo: Repo
}

type ident = symbol | string | number

export interface Projects extends Record<ident, Project> {}
